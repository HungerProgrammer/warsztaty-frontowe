# RWD &ndash; Grid

### Tworzenie grida (~ 10min - 15min)

Stwórz 12-kolumnowy grid za pomocą Sass. Odpowiednio zagnieźdź elementy tak, aby nie można było użyć kolumn bez wierszy ani bez głównego kontenera. Wykorzystaj grid do ustawienia obok siebie 3 elementów.


### Zadanie 1. Strona w układzie grid

Przygotuj prostą stronę w oparciu o układ kolumnowy, odwzoruj poniższy obraz. Skorzystaj z grida, który stworzyliście wcześniej z wykładowcą.

![grid](images/grid.jpg)
