# RWD &ndash; Mobile first

### Zadanie 1. Mixin, mobile first

W pliku `index.html` znajdziesz sekcję o klasie `container`, a w niej element `div` o klasie `sidebar` oraz element `section`.  Użyj podejścia mobile first.
Ustaw te elementy w następujący sposób:

Ekrany poniżej 600px:

![Mobile](images/mobile.png)

Ekrany powyżej 600px:
![PC](images/pc.png)

### Zadanie 2. Viewport
Ustaw viewport dla Twojej strony tak, aby zawsze był taki jak szerokość urządzenia, a domyślne powiększenie miało wartość **1**.
Zmień wartość atrybutu **content** na `width=500`, a następnie na `width=100`.
Sprawdzaj stronę za pomocą podglądu mobilnego w narzędziach developerskich przeglądarki.

### Zadanie 3. Zapytania media queries
Ustaw media queries zgodnie z poniższą tabelą.

| Wielkość ekranu w pikselach | Kolor tła |
| :---: | :---: |
| do 400 | zielony |
| od 400 do 800 | fioletowy |
| ponad 800 | pomarańczowy |

### Zadanie 4. Ustawienia druku
Zobacz jak wygląda przepis na stronie Kwestia Smaku
https://www.kwestiasmaku.com/przepis/makaron-ze-szpinakiem

A następnie zwróć uwagę jak wygląda ten sam przepis w podglądzie wydruku(**CTRL+P**).

Stwórz swoją stronę z menu(**1 zadanie**) opisem do zdjęcia(**zadanie 2 Pozycje**). A następnie dostosuj ją w ten sposób, żeby w druku została jedynie część przepisem i zdjęciem.
